package sbu.Projects;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Tweet {
    private String tweet;
    private Date time;
    private ArrayList<String> like;
    private Profile account;
    private int id;

    public static int count = 1000;
    public Tweet(String tw , Profile pro){
        time = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        formatter.format(time);
        tweet = tw;
        like = new ArrayList<>();
        account = pro;
        id = ++count;
    }

    public void showTweet(){
        System.out.println(" from: " + account.getName() + "  @" + account.getId() + "\n "
                            + time.toString() + "\n " + "tweet id: " + id + "\n  " + tweet);
        if(like.size() < 1000) {
            System.out.print(like.size());
        }
        else if(like.size() > 999){
            System.out.print(like.size()/1000 + "k");
        }
        System.out.print(" likes");
    }

    public int getId(){return id;}

    public void addLike(Profile pro){
        if(!like.contains(pro.getId())) {
            like.add(pro.getId());
            System.out.println("You like tweet " + id);
        }
        else{
            System.out.println("You have already liked tweet " + id);
        }
    }

    public void unLike(Profile pro){
        if(like.contains(pro.getId())) {
            like.remove(pro.getId());
            System.out.println("You unlike tweet " + id);
        }
        else{
            System.out.println("You did not like tweet " + id);
        }
    }

    public Profile getAccount() {
        return account;
    }
}
