package sbu.Projects;
/* this class is for login/sign up*/

import java.util.Scanner;

public class Enter{
    public static void login(){
        Scanner inp = new Scanner(System.in);
        System.out.print("username:");
        String usr = inp.next();
        System.out.print("password:");
        String psw = inp.next();
        short statue = Platform.login(usr , psw);
        if(statue == 1){
            System.out.println("\nYou logged in!");
        }
        else if(statue == -1){
            System.out.println("\nYour password is incorrect!");
        }
        else if(statue == 0){
            System.out.println("\nThis username has not exist!");
        }
    }

    public static void signUp(){
        Scanner inp = new Scanner(System.in);
        System.out.print("username:");
        String usr = inp.next();
        System.out.print("password:");
        String psw = inp.next();
        short statue = Platform.signUp(usr , psw);
        if(statue == 1 || statue == -1){
            System.out.println("This username has already exist!");
        }
        else if(statue == 0){
            System.out.println("Your account is created!");
        }
    }
}
