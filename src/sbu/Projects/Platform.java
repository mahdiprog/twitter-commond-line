package sbu.Projects;

/* this class is the connection of all classes and save list of tweets and profiles */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Platform {
    protected static ArrayList<MyProfile> profiles = new ArrayList<MyProfile>(); //list of all profiles that exist
    protected static ArrayList<Tweet> allTweets = new ArrayList<>();  //list of all tweets that exist
    private static int nAccount = -1;   //determine now which account logged in

    /***************exist account***************/

    // if return 1 it means has exist
    // if return -1 it means email has exist but password is incorrect
    // if return 0 it means that email has not exist at all
    public static short has_exist(String usr , String psw){
        for (MyProfile pro : profiles){
            if(pro.getId().equals(usr)){
                if(pro.checkPassword(psw)){
                    return 1;
                }
                else {
                    return -1;
                }
            }
        }
        return 0;
    }

    /***************log in***************/

    public static short login(String usr , String psw){
        short statue = has_exist(usr , psw);
        if(statue == 0 || statue == -1){
            return statue;
        }
        for (int i = 0 ; i < profiles.size() ; i++) {
            if(profiles.get(i).getId().equals(usr)){
                nAccount = i;
                break;
            }
        }
        return statue;
    }

    /***************sign Up***************/

    public static short signUp(String usr , String psw){
        short statue = has_exist(usr , psw);
        if(statue == 1 || statue == -1){
            return statue;
        }
        else {
            int tail = profiles.size();
            profiles.add(new MyProfile(usr , psw));
            profiles.get(tail).creat();
            //accounts.put(usr , psw);
        }
        return statue;
    }

    /***************log out***************/

    public static boolean logout(){
        if(nAccount != -1){
            nAccount = -1;
            System.out.println("You logged out!");
            return true;
        }
        System.out.println("You has not logged in yet!");
        return false;
    }

    /***************make tweet***************/

    public static void maketweet(){
        if(nAccount == -1){
            System.out.println("You are guest!\nYou should log in first!");
            return;
        }
        String inp = "";
        System.out.println("What\'s happening? ");
        Scanner input = new Scanner(System.in);
        inp += input.nextLine();
        //check length of string that is valid
        if(inp.length() > 140){
            System.out.println("Error! Your tweet should be under 140 characters!");
        }
        else {
            // make a new tweet
            Tweet tw =new Tweet(inp , (Profile) profiles.get(nAccount));

            allTweets.add(tw);  //add to all tweets that exist
            profiles.get(nAccount).addTweet(tw);    //add to this account tweets
            profiles.get(nAccount).addTimelineTweets(tw); //add to timeline of account
            // add to timeline of its followers
            for (Profile pro:profiles.get(nAccount).getFollowers()) {
                pro.addTimelineTweets(tw);
            }
            System.out.println("Your tweet has been sent!");
        }
    }

    /***************show profile***************/

    public static void showMyProfile(){
        if(nAccount == -1){
            System.out.println("You are guest!\nYou should log in first!");
            return;
        }
        profiles.get(nAccount).viewProfile();
    }
    public static boolean showProfile(){
        System.out.println("Please enter id:");
        Scanner inp = new Scanner(System.in);
        String input = inp.next();
        //find account
        for (Profile pro: profiles) {
            if(pro.getId().equals(input)) {
                pro.viewProfile();
                return true;
            }
        }
        return false;
    }

    /***************show followers followings***************/

    public static void showMyFollowers(){
        if(nAccount == -1){
            System.out.println("You are guest!\nYou should log in first!");
            return;
        }
        profiles.get(nAccount).showFollowers();
    }
    public static void showMyFollowings(){
        if(nAccount == -1){
            System.out.println("You are guest!\nYou should log in first!");
            return;
        }
        profiles.get(nAccount).showFollowings();
    }

    /***************timeline***************/

    public static void timeline(){
        if(nAccount == -1){
            System.out.println("You are guest!\nYou should log in first!");
            return;
        }
        profiles.get(nAccount).showTimeline();
    }

    /***************follow***************/

    public static void follow(){
        boolean exist = false;
        if(nAccount == -1){
            System.out.println("You are guest!\nYou should log in first!");
            return;
        }
        System.out.println("Enter id account:");
        Scanner inp = new Scanner(System.in);
        String input = inp.next();
        //check is not himself account
        if(input.equals(profiles.get(nAccount).getId())){
            System.out.println("\nYou can not follow or unfollow yourself account!");
            return;
        }
        //find account
        for (Profile pro: profiles){
            if(pro.getId().equals(input)){
                exist = true;
                if(!profiles.get(nAccount).follow(pro)){
                    System.out.println("You already have followed @" + input + "!");
                }
                else {
                    System.out.println("Now you are follower of @" + input + "!");
                    updateTimeline(pro , true);
                }
            }
        }
        if (!exist){
            System.out.println("@" + input + " does not found!");
        }
    }

    /***************unfollow***************/

    public static void unfollow(){
        if(nAccount == -1){
            System.out.println("You are guest!\nYou should log in first!");
            return;
        }
        System.out.println("Enter id account:");
        Scanner inp = new Scanner(System.in);
        String input = inp.next();
        //check is not himself account
        if(input.equals(profiles.get(nAccount).getId())){
            System.out.println("\nYou can not follow or unfollow yourself account!");
            return;
        }
        //find account
        for (Profile pro: profiles){
            if(pro.getId().equals(input)){
                if(!profiles.get(nAccount).unfollow(pro)){
                    System.out.println("You have not followed @" + input + " yet!");
                }
                else {
                    System.out.println("successfully unfollow @" + input + "!");
                    updateTimeline(pro , false);
                }
            }
        }
    }

    /***************like***************/
    public static void like(){
        if(nAccount == -1){
            System.out.println("You are guest!\nYou should log in first!");
            return;
        }
        System.out.println("Enter id tweet:");
        Scanner inp = new Scanner(System.in);
        int input = inp.nextInt();
        if(allTweets.size() <= (input - 1001)){
            System.out.println("This tweet with id " + input + " does not exist!");
            return;
        }
        allTweets.get(input - 1001).getAccount().addlikeTimeline(allTweets.get(input - 1001).getId() , profiles.get(nAccount));
    }

    public static void unLike(){
        if(nAccount == -1){
            System.out.println("You are guest!\nYou should log in first!");
            return;
        }
        System.out.println("Enter id tweet:");
        Scanner inp = new Scanner(System.in);
        int input = inp.nextInt();
        if(allTweets.size() <= (input - 1001)){
            System.out.println("This tweet with id " + input + " does not exist!");
            return;
        }
        allTweets.get(input - 1001).getAccount().addUlikeTimeline(allTweets.get(input - 1001).getId() , profiles.get(nAccount));
    }

    public static void updateTimeline(Profile profile , boolean add){
        ArrayList<Tweet> Tmypro= profiles.get(nAccount).getTimelineTweets();
        ArrayList<Tweet> Tpro = profile.getMyTweets();
        if(add) {
            int i;
            boolean is_added;
            for (Tweet tw : Tpro) {
                i = 0;
                is_added = false;
                for (Tweet time : Tmypro) {
                    if (time.getId() > tw.getId()) {
                        profiles.get(nAccount).addTimelineTweets(i, tw);
                        is_added = true;
                        break;
                    }
                    i++;
                }
                if (!is_added) {
                    profiles.get(nAccount).addTimelineTweets(tw);
                }
            }
        }
        else{
            for (Tweet tw : Tpro) {
                profiles.get(nAccount).removeTweetTimeline(tw);
            }
        }
    }
}