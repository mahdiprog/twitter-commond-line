package sbu.Projects;

import java.util.ArrayList;
import java.util.Scanner;

public class Profile {
    protected String name;
    protected String id;
    protected String bio;
    protected ArrayList<Profile> followers = new ArrayList<>();
    protected ArrayList<Profile> followings = new ArrayList<>();
    protected ArrayList<Tweet> myTweets = new ArrayList<>();
    protected ArrayList<Tweet> timelineTweets = new ArrayList<>();

    /***************add tweet its tweet***************/

    public void addTweet(Tweet tweet){myTweets.add(tweet);}

    /***************add tweets to timeline***************/

    public void addTimelineTweets(Tweet tweet){timelineTweets.add(tweet);}

    public void addTimelineTweets( int n , Tweet tweet){timelineTweets.add(n , tweet);}

    public ArrayList<Profile> getFollowings() {return followings;}

    public ArrayList<Profile> getFollowers() {return followers;}

    /***************show profile***************/
    public void viewProfile(){
        System.out.println("\n " + name + "\n @" + id + "\n " + bio + "\n " + myTweets.size() + " tweet\t");
        if(followers.size() < 1000) {
            System.out.print(followers.size());
        }
        else if(followers.size() > 999 && followers.size() < 1000000){
            System.out.print(followers.size()/1000 + "k");
        }
        System.out.print(" folowers\t");

        if(followings.size() < 1000) {
            System.out.print(followings.size());
        }
        else if(followings.size() > 999 && followings.size() < 1000000){
            System.out.print(followings.size()/1000 + "k");
        }
        System.out.print(" followings\t");

        for (int i = myTweets.size() ; i > 0 ; i--) {
            System.out.println("\n---------------------------\n");
            myTweets.get(i-1).showTweet();
        }
        System.out.println("\n---------------------------\n");
    }

    /***************show followers***************/

    public void showFollowers(){
        System.out.print("Followers:");
        System.out.println("\n---------------------------\n");
        for (Profile follower: followers){
            System.out.println(" " + follower.name + "\t@" + follower.id);
            System.out.println("\n---------------------------\n");
        }
    }

    /***************show followings***************/

    public void showFollowings(){
        System.out.print("Followings:");
        System.out.println("\n---------------------------\n");
        for (Profile following: followings){
            System.out.println(" " + following.name + "\t@" + following.id);
            System.out.println("\n---------------------------\n");
        }
    }

    /***************show timeline***************/

    public void showTimeline(){
        for (int i = timelineTweets.size() ; i > 0 ; i--) {
            System.out.println("\n---------------------------\n");
            timelineTweets.get(i-1).showTweet();
        }
        System.out.println("\n---------------------------\n");
    }

    /***************add follow***************/

    public boolean follow (Profile pro){
        if(followings.contains(pro)){
            return false;
        }
        followings.add(pro);
        pro.addFollower(this);
        return true;
    }

    public void addFollower(Profile pro){
        this.followers.add(pro);
    }

    /***************unfollow***************/

    public boolean unfollow (Profile pro){
        if(followings.contains(pro)){
            followings.remove(pro);
            pro.removeFollower(this);
            return true;
        }
        return false;
    }

    public void removeFollower(Profile pro){

        this.followers.remove(pro);
    }
    /***************update timeline***************/

    public void addlikeTimeline(int id , Profile pro){
        for (Tweet tw: myTweets){
            if(tw.getId() == id){
                tw.addLike(pro);
            }
        }
    }

    public void addUlikeTimeline(int id , Profile pro){
        for (Tweet tw: myTweets){
            if(tw.getId() == id){
                tw.unLike(pro);
            }
        }
    }

    public void removeTweetTimeline(Tweet tw){timelineTweets.remove(tw);}

    public String getName() {return name;}
    public String getId() {return id;}
    public String getBio() {return bio;}
    public ArrayList<Tweet> getTimelineTweets() {return timelineTweets;}
    public ArrayList<Tweet> getMyTweets() {return myTweets;}
}