package sbu.Projects;

/*this function get commond and call related function */

import java.util.Scanner;

public class CommondLine {
    public String input;

    //this function get input
    public void getCommond(){
        Scanner cl = new Scanner(System.in);
        do {
            System.out.print(">");
            input = "";
            input += cl.nextLine();
            if(input.equals("quit")){
                break;
            }
            if(!check(input.trim())){
                System.out.println("commond " + input + " did not defined!");
            }
        }while(true);
    }

    //this function check and call related function
    public boolean check(String commond){
        if(commond.equals("login")){
            Enter.login();
            return true;
        }
        else if(commond.equals("sign up")){
            Enter.signUp();
            return true;
        }
        else if(commond.equals("logout")){
            Platform.logout();
            return true;
        }
        else if(commond.equals("tweet")){
            Platform.maketweet();
            return true;
        }
        else if(commond.equals("my profile")){
            Platform.showMyProfile();
            return true;
        }
        else if(commond.equals("profile")){
            Platform.showProfile();
            return true;
        }
        else if(commond.equals("followers")){
            Platform.showMyFollowers();
            return true;
        }
        else if(commond.equals("followings")){
            Platform.showMyFollowings();
            return true;
        }
        else if(commond.equals("timeline")){
            Platform.timeline();
            return true;
        }
        else if(commond.equals("follow")){
            Platform.follow();
            return true;
        }
        else if(commond.equals("unfollow")){
            Platform.unfollow();
            return true;
        }
        else if(commond.equals("like")){
            Platform.like();
            return true;
        }
        else if(commond.equals("unlike")){
            Platform.unLike();
            return true;
        }
        return false;
    }
}
